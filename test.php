<?php

use App\usersRoute;
use Slim\Http\Environment;
use Slim\Http\Request;
use PHPUnit\Framework\TestCase;
class test extends TestCase
{
    //vendor\bin\phpunit test.php --stder
    protected $app;
    public function setUp()
    {
                $jdb  = new App\Jsondb();
        $jdb ->connect();
        $this->app = (new usersRoute($jdb))->get();
    }
                public function testPostAll() {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'get',
            'REQUEST_URI'    => '/paymentall',
            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
        ]);
        $req = Request::createFromEnvironment($env)->withParsedBody([]);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["message"], null);
        session_destroy();
        }
        
        public function testPostRegister() {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI'    => '/register',
            'QUERY_STRING' => 'purpose=purpose&sum=100',
            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
        ]);
        $req = Request::createFromEnvironment($env)->withParsedBody([]);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 302);
        $headers = $response->getHeaders();
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["message"], null);
        
      $env = Environment::mock([
            'REQUEST_METHOD' => 'get',
            'REQUEST_URI'    => $headers['Location'][0],
            'QUERY_STRING' => 'purpose=purpose&sum=100',
            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
        ]);
        $req = Request::createFromEnvironment($env)->withParsedBody([]);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        session_destroy();
    } 
     public function testPostdet() {
          session_start();
          $js = array();
          $js['cnum'] = '444444444444444444';
        $env = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI'    => '/detect',

        ]);
        $req = Request::createFromEnvironment($env)->withParsedBody($js);

        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 404);
        $headers = $response->getHeaders();
        $result = json_decode($response->getBody(), true);
        //$this->assertSame($result["message"], 'bad number');
        session_destroy();
     }
     
          public function testPostdettrue() {
          session_start();
          $js = array();
          $js['cnum'] = '5404368693739999';
        $env = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI'    => '/detect',

        ]);
        $req = Request::createFromEnvironment($env)->withParsedBody($js);

        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $headers = $response->getHeaders();
        $result = json_decode($response->getBody(), true);
        //$this->assertSame($result["message"], 'bad number');
        session_destroy();
     }
    
}
?>