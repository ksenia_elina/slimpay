<?php
namespace App;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Views\PhpRenderer;

class usersRoute
{
    /**
     * Stores an instance of the Slim application.
     *
     * @var \Slim\App
     */
    private $app;
    public $jdb;
    public function __construct(Jsondb $jdb) {
                $lifetime=1800;
        session_set_cookie_params($lifetime);
        session_start();
        $SI = session_id();
        $jdb ->connect();
        $app = new \Slim\App();
        $container = $app->getContainer();
        $container['renderer'] = new PhpRenderer("./temp/");
        $container['myFunction'] = function($container) {
            return function($card_number) {
                $card_number_checksum = '';

                foreach (str_split(strrev((string) $card_number)) as $i => $d) {
                    $card_number_checksum .= $i %2 !== 0 ? $d * 2 : $d;
                }
                
                return array_sum(str_split($card_number_checksum)) % 10 === 0;
        
    
            };
        };

        $app->get('/', function (Request $request, Response $response) use($container){

            return $container['renderer']->render($response, "/tcpatch.php");
            return $response7;
        });
        
        $app->post('/register', function (Request $request, Response $response) use($container,$SI){

            //Construct the View
            $phpView = new PhpRenderer("/");

            //$SI = uniqid('', true);
            $data = $request->getParsedBody();
            $_SESSION['purpose'] = filter_var($data['purpose'], FILTER_SANITIZE_STRING);
            $_SESSION['sum'] = $data['sum'];
            return $response->withRedirect('./payment/'.$SI);
        });
        
        $app->get('/payment/{sessionid}', function (Request $request, Response $response) use ($container) {
            
            return $container['renderer']->render($response, "/pf.php");
        });
        $app->get('/paymentall', function (Request $request, Response $response) use ($container,$jdb) {
            $all = $jdb ->get('lib', true);
             If(empty($all))
            {
                $res = array();
                $res['msg'] = 'nofile';
                $res['error'] = true;
                $res['status'] = 404;
            }
            else
            {
                $res = array();
                $res['msg'] = $jdb ->get('lib', true);
                $res['error'] = false;
                $res['status'] = 200;
            }
            return $response->withJson($res);
        });
        $app->post('/detect', function (Request $request, Response $response) use($container,$SI,$jdb){
                
            $data = $request->getParsedBody();
            $_SESSION['cnum'] = filter_var($data['cnum'], FILTER_SANITIZE_STRING);
            var_dump($data['cnum']);
            $myFunc = $this->myFunction;
            //var_dump($myFunc('5404368693739999'));
            //var_dump($myFunc($_SESSION['cnum']));
            If($myFunc($data['cnum']))
            {
                $res = array();
                $res['msg'] = 'ok';
                $res['error'] = false;
                $res['status'] = 200;
                $settings['table'] = 'lib';
                $settings['values'] = array();
                $settings['values']['purpose'] = $_SESSION['purpose'];
                $settings['values']['sum'] = $_SESSION['sum'];
                $settings['values']['cnum'] = $_SESSION['cnum'];
                $id = $jdb ->insert('lib',$settings);
                //return $response->withJson($res, 200);
            }
            else
            {
                $res = array();
                $res['msg'] = 'bad number';
                $res['error'] = true;
                $res['status'] = 404;
                //return $response->withJson($res, 404);
            }

            return $response->withJson($data['cnum'], $res['status']);

        });

        $this->app = $app;
    }

    /**
     * Get an instance of the application.
     *
     * @return \Slim\App
     */
    public function get()
    {
        return $this->app;
    }
}